import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/widgets.dart' as widgets;
import 'package:http/http.dart' as http;
import 'ocr.dart';

Future<String> getImage(String url) async {
  var dir = Directory.systemTemp.createTempSync();
  var path = "${dir.path}/tmp_image.jpg";
  var file = File(path);
  file.createSync();
  final uri = Uri.parse(url);
  await http.get(uri).then((response) {
    file.writeAsBytes(response.bodyBytes);
  });
  return path;
}

Future<Coupon> createCouponFromImagePath(String path) async {
  final File imageFile=File(path);
  await Firebase.initializeApp();
  final barcode = await getBarcode(imageFile);

  Coupon coupon = Coupon()
    ..photoPath = path
    ..barcode = barcode
    ..image = imageFile
    ..discount = 0;
  return coupon;
}

Future<Coupon> createCoupon(Map<String, dynamic> basicData) async {
  var data = basicData["coupons"][0];
  String url = data["photo"]["url"];
  String imagePath = await getImage(url);

  return Coupon()
    ..photoPath = imagePath
    ..barcode = data["barcode"]
    ..image = File(imagePath)
    ..discount = 0;
}

class Coupon
{
  widgets.Image img;
  File image;
  String photoPath;
  String barcode;
  DateTime dateFrom;
  DateTime dateTo;
  double discount;
}
