import 'package:flutter/material.dart';
import 'db.dart';
import 'coupon.dart';
import 'package:barcode_widget/barcode_widget.dart';

Future<String> getBarcodeFromCoupon() async
{
  final coupon = await DbHelper.db.getLastCoupon();
  return coupon.barcode;
}

class BarcodeCouponScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<Coupon>(
          future: DbHelper.db.getLastCoupon(),
          builder: (context, snapshot){
            if (snapshot.connectionState == ConnectionState.done) {
              return BarcodeScreen(
                  title: 'Kod kuponu',
                  text: "Zeskanuj kod kuponu i zatwierdź przyciskiem w prawym dolnym rogu",
                  barcode: snapshot.data.barcode,
                  icon: 'assets/recipt.png',
                  buttonCallback:() {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                  }
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.check),
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          }),
    );
  }
}

class BarcodeCardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BarcodeScreen(
        title: 'Kod karty',
        text: "Zeskanuj poniższy kod karty i zatwierdź przyciskiem w prawym dolnym rogu",
        barcode: '9954695064155',
        icon: 'assets/card.png',
        buttonCallback: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => BarcodeCouponScreen(),
            ),
          );
        });
  }
}

Container customBarcode(String barcode)
{
  return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          color: Colors.white
      ),
      child: BarcodeWidget(
        barcode: Barcode.ean13(),
        data: barcode,
        color: Colors.black,
        padding: EdgeInsets.all(20),
        style: TextStyle(color: Colors.black),
      )
  );
}

class BarcodeScreen extends StatelessWidget {
  final String title;
  final String text;
  final String barcode;
  final icon;
  final buttonCallback;
  const BarcodeScreen({Key key, this.title, this.text, this.barcode, this.buttonCallback, this.icon});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(this.title)),
        body: Column(
          children:[
            Flexible(
              flex: 4,
              fit: FlexFit.tight,
              child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:[
                  Flexible(
                      flex:3,
                      child:Image.asset(this.icon)),
                  Spacer(flex:1),
                  Flexible(
                      flex: 6,
                      child:Text(this.text))],
              ),
            ),
            Spacer(flex:1),
            Flexible(
                flex:10,
                child:customBarcode(this.barcode)
            ),
            Spacer(flex:3)
          ],
        ),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.check),
            onPressed: this.buttonCallback
        )
    );
  }
}