import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:barcode_widget/barcode_widget.dart';

import 'coupon.dart';
import 'db.dart';

class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;

  const TakePictureScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(widget.camera, ResolutionPreset.medium,
        enableAudio: false);
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Zrób zdjęcie kuponu')),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        onPressed: () async {
          try {
            await _initializeControllerFuture;
            final image = await _controller.takePicture();
            var db = GraphCms(graphCmsUrl, token);
            var coupon = await createCouponFromImagePath(image?.path);
            db.addCoupon(coupon);
            printSnack("Kod ${coupon.barcode} został dodany", context);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    DisplayPictureScreen(
                      imagePath: image?.path,
                      barcode: coupon.barcode,
                    ),
              ),
            );
          } catch (e) {
            printSnackError(e.toString(), context);
          }
        },
      ),
    );
  }
}
void printSnackError(String text, BuildContext context) {
  final snackBar = SnackBar(
      content: Text(text, style: TextStyle(
          color: Colors.white)
      ),
      backgroundColor: Colors.red);
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

void printSnack(String text, BuildContext context) {
  final snackBar = SnackBar(
      content: Text(text)
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;
  final String barcode;

  const DisplayPictureScreen({Key key, this.imagePath, this.barcode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Zeskanowany kupon')),
        body: Column(children: [
          BarcodeWidget(
              barcode: Barcode.code128(),
              data: barcode,
              color: Colors.white),
          Flexible(child:Image.file(File(imagePath)))
        ]),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.check),
            onPressed: () {
              Navigator.popUntil(context, (route) => route.isFirst);
            })
    );
  }
}
