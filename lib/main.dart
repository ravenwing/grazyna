import 'dart:async';

import 'barcode.dart';
import 'camera.dart';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';


CameraDescription camera;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  camera = cameras.first;

  runApp(
    MaterialApp(
        theme: ThemeData.dark(),
        // home: TakePictureScreen(camera: firstCamera),
        home: MainMenuScreen()),
  );
}

class MainMenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
          TextButton(
              child: Text('Wykorzystaj kupon'),
              style: TextButton.styleFrom(
                backgroundColor: Colors.teal,
                minimumSize: Size(50, 100),
                primary: Colors.white,
                textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => BarcodeCardScreen()));
              }),
          SizedBox(height: 50),
          TextButton(
              child: Text('Zrób zdjęcie kuponu'),
              style: TextButton.styleFrom(
                backgroundColor: Colors.teal,
                minimumSize: Size(50, 100),
                primary: Colors.white,
                textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            TakePictureScreen(camera: camera)));
              }),
        ]));
  }
}
