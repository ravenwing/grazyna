import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'dart:io';

class NoBarcodeException implements Exception {
  @override
  String toString() => "Brak kodów kreskowych na zdjęciu!";
}

class TooMuchCodesException implements Exception {
  @override
  String toString() => "Więcej niż jeden kod kreskowy na zdjęciu!";
}

Future<String> getBarcode(File imageFile) async {
  final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(
      imageFile);
  final BarcodeDetector barcodeDetector = FirebaseVision.instance
      .barcodeDetector();
  final List<Barcode> barcodes = await barcodeDetector.detectInImage(
      visionImage);
  if (barcodes.isEmpty)
    throw NoBarcodeException();
  if (barcodes.length>1)
    throw TooMuchCodesException();
  final String barcode = barcodes[0].rawValue;
  return barcode;
}