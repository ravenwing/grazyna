import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:graphql/client.dart';

import 'coupon.dart';

final graphCmsUrlBase = "http://api-eu-central-1.graphcms.com";
final String graphCmsUrl =
    "https://api-eu-central-1.graphcms.com/v2/cknnlx24yfjmx01z1ccw73gzk/master";
final String token =
    'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImdjbXMtbWFpbi1wcm9kdWN0aW9uIn0.eyJ2ZXJzaW9uIjozLCJpYXQiOjE2MTg3Nzg5MDUsImF1ZCI6WyJodHRwczovL2FwaS1ldS1jZW50cmFsLTEuZ3JhcGhjbXMuY29tL3YyL2Nrbm5seDI0eWZqbXgwMXoxY2N3NzNnemsvbWFzdGVyIiwiaHR0cHM6Ly9tYW5hZ2VtZW50LW5leHQuZ3JhcGhjbXMuY29tIl0sImlzcyI6Imh0dHBzOi8vbWFuYWdlbWVudC5ncmFwaGNtcy5jb20vIiwic3ViIjoiZTMyNjMwYjYtZjhjMC00NTUwLWE5OTktNjIzYjhhNDlkNmRlIiwianRpIjoiY2tubm41eHY1Z2pkeDAxejE4ODlpaDR6aiJ9.pxJL6-EyAyB2Cu0W3Q8Td0cW9gwVKT684AicwqOMpL6V3ehHVDtWWVfzuAz4owDz0qkiuTDJsgtCjo73REUrguTi_1ePHeq-yTuYQvk22-Wt6kh1YmQkZ2ppCPTJd6M_j7_vKnNKeJHq8lqXWI6SJNLfyicrsfQsaDnFM4G2u8ipxfyygqh8YBCTjnEZZEcBUGmk-imqmhPzU3ui4qXcvrzPzalsmychTYu-EZaPwPALQHeY3A6fzBOTxfkRkWAHpId3fiVgBae58BtlOCbwISH8PSA1Q7-Kkn9UtyGANRfHAga32DwxJL2r8IoRuUUAzdgP1PGtc5PNPwxhvIUt05J7mANP2FViqTKSTZ5tM5Z8sgjo-OAk42PfP30UU3aPMMqdycc-to18sSlL6qeuMkHnD2nDLlnXWbUMQhhuLdDfRUVn1itlGTbZjwqoe--vjkOhvzx6KIG7cAF2zYcADAro_WH8KQ7HIYRDLSIUDTOS0DuGfI8Qcp3hOvBTJwrlqbDtecAJfoQxsPjlfo7ghqulcBWRD-BTvHKaP5MZv438-UWt1Qcib6mAGC-XdfB8JVJBApbV59WPObBKJadO_kWU_vVkcuPL1hFwq370QxDZx1OtrPpFn-AgwMFHvs2noNic2zNtgW5WfckQqQOx651q7jId_UD7gFyX6vAqBX8';

class DbHelper {
  static final Database db = GraphCms(graphCmsUrl, token);
}

abstract class Database {
  void addCoupon(Coupon coupon);
  Future<Coupon> getLastCoupon();
}


class GraphCms implements Database {
  String graphCmsUrl;
  String token;
  GraphQLClient client;

  GraphCms(String graph, String token)
  {
    this.graphCmsUrl = graph;
    this.token = token;
    final HttpLink httpLink = HttpLink(this.graphCmsUrl);
    final AuthLink authLink = AuthLink(getToken: () async => token);
    final Link link = authLink.concat(httpLink);
    this.client = GraphQLClient(link: link, cache: GraphQLCache());
  }

  Future<String> uploadPhoto(String photoPath) async {
    var request =  http.MultipartRequest("POST", Uri.parse(graphCmsUrl + "/upload"));
    request.headers['Authorization'] = token;
    var file = await http.MultipartFile.fromPath('fileUpload', photoPath);
    request.files.add(file);
    final result = await request.send();
    var body = await result.stream.bytesToString();
    var jsonBody = jsonDecode(body);
    return jsonBody["id"];
  }

  Future<QueryResult> getLastCouponEntry() async
  {
    QueryOptions options = QueryOptions(
        document: gql(getLastCouponQuery));
    QueryResult result =  await client.query(options);
    return result;
  }

  @override
  void addCoupon(Coupon coupon) async
  {
    Future<String> photoId = uploadPhoto(coupon.photoPath);
    QueryOptions options = QueryOptions(
        document: gql(addCouponQuery),
        variables: <String, dynamic>{
          'id': await photoId,
          'barcode': coupon.barcode
        });
    client.query(options);
  }

  @override
  Future<Coupon> getLastCoupon() async {
    var result = await getLastCouponEntry();
    Map<String, dynamic> valueMap = result.data;
    return createCoupon(valueMap);
  }

}
const String addCouponQuery = r'''
mutation MyMutation ($id: ID!, $barcode: String!){
createCoupon(data:{ barcode: $barcode, photo: { connect: {id: $id}}})
{
    id
    barcode

}
}''';

const String getLastCouponQueryOld = r'''
  query MyQuery {
    assets(last: 1) {
      url
    }
  }''';

const String getLastCouponQuery = r'''
  query MyQuery {
  coupons(last: 1)
  {
    id
    barcode
    photo
    {
      url
    }
  }
}''';
